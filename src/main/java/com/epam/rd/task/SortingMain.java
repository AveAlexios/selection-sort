package com.epam.rd.task;
//to prove that the algorithm can work with INT, CHAR and DOUBLE
// I created 3 separate methods, each with one of those algorithms with specific data type
// (data types in them are completely interchangeable)
// Since I didn't know how to set a universal randomizer and universal methods
//The code may look too overloaded - I don't know hot to simplify it YET

public class SortingMain {
    public static void main(String[] args) {

//Creating an array of INTs and using insertion sorting on that
        int[] arrayOfIntegers = new int[] {8, 0, -3, -92, 2, 12, 42, 42};
        for (int i : arrayOfIntegers) {
            System.out.print(i + " ");
        }System.out.println();

        InsertionSortingAlgorithm.sort(arrayOfIntegers);
        for (int i : arrayOfIntegers) {
            System.out.print(i + " ");
        }System.out.println('\n');

//Creating an array of CHARs and using Bubbles sorting on that
        char[] arrayOfCharacters = new char[] {'S', 'a', 'b', 'A', 'z', 'H', 'y'};
        for (char i : arrayOfCharacters) {
            System.out.print(i + " ");
        }System.out.println();

        BubblesAlgorithm.sort(arrayOfCharacters);
        for (char i : arrayOfCharacters) {
            System.out.print(i + " ");
        }System.out.println('\n');

//Creating an array of DOUBLEs and using Direct Selection sorting on that
        double[] arrayOfDoubles = new double[] {2.3, 21.3, 0.000001, -0.12, -32.0, 3.14, -1.00010};
        for (double i : arrayOfDoubles) {
            System.out.print(i + " ");
        }System.out.println();

        DirectSelectionAlgorithm.sort(arrayOfDoubles);
        for (double i : arrayOfDoubles) {
            System.out.print(i + " ");
        }System.out.println();

    }
}

