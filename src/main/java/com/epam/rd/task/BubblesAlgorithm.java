package com.epam.rd.task;
public class BubblesAlgorithm {
    public static void sort(char[] arr) {
        boolean isSorted = false;
        char buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length-1; i++) {
                if(arr[i] > arr[i+1]){
                    isSorted = false;
                    buf = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = buf;
                }
            }
        }
    }
}
